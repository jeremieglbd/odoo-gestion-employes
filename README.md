Module Gestion des Ressources Humaines du Groupement Employeur de la Vienne
---

This module is developed during a master's degree project.
It is able to manage human resources for the Vienne GE.

The module is forked from https://apps.odoo.com/apps/modules/13.0/laundry_management/
